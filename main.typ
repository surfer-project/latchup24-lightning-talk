#import "typst-presentation-common/theme.typ": *
#import "typst-presentation-common/highlight.typ": *
#import "@preview/cades:0.3.0": qr-code

#set text(size: 25pt, font: "Noto Sans")

#show strong: set text(fill: rgb(75%, 100%, 80%))

#show: university-theme.with(
  progress-bar: false
)

#show: highlights

#let both_logos(height) = [#box(image("liu_logo.svg", height: height))]

// #show raw.where(lang: "spade"): it => [
//   #show regex("\b(entity|let|inst|end)\b") : keyword => text(weight:"bold", keyword)
//   #it
// ]

#slide(title: "Conclusions")[
  Surfer is a *snappy* and *extensible* waveform viewer that *runs everywhere*

  #let both_logos = box(
    height: 2cm,
    stack(dir: ltr, [#image("./fig/liu_logo.svg")], [#image("./fig/JKU_Logo.svg")])
  )

  #uncover(3, [#stack(dir: ltr, stack(dir: ttb, [#v(3cm) #h(6cm) Try it on your phone!], v(2cm), both_logos), h(2cm), [
    // TODO: Register this
    #qr-code("latchup24.surfer-project.org", width: 7cm)
  ])])
]

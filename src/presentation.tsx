import init from "../surfer/dist/surfer.js"
import {CONFIG} from "./surfer_config.ts"
await init('../surfer/dist/surfer_bg.wasm');
import { inject_message, waves_loaded, id_of_name, index_of_name, spade_loaded } from '../surfer/dist/surfer.js';

function bigInt(t: number) {
    return [1, [t]];
}

async function sleep(time: number) {
    return new Promise(r => setTimeout(r, time));
}

function inject_object(o: any): void {
    inject_message(JSON.stringify(o))
}

function inject_objects(os: any[]): void {
    inject_object({ Batch: os })
}

async function waitingKeypress() {
    return new Promise((resolve) => {
        document.addEventListener('keydown', onKeyHandler);
        function onKeyHandler(e) {
            if (e.keyCode === 39 || e.keyCode === 40) {
                document.removeEventListener('keydown', onKeyHandler);
                resolve();
            }
        }
    });
}

function signal(path: string[], name: string): any {
    return { path: { strs: path }, name: name }
}

class GraphicsManager {
    async add_text_arrow(
        slideman: SlideManager,
        from_name: string,
        from_time: number,
        from_direction: string,
        to_name: string,
        to_time: number,
        to_direction: string,
        text: string,
    ): Promise<number> {
        let from_id = await id_of_name(from_name)
        let to_id = await id_of_name(to_name)

        const anchor_from_dir = (direction, is_from) => {
            if (direction == "North") {
                if (is_from) {
                    return "Bottom"
                } else {
                    return "Top"
                }
            } else if (direction == "South") {
                if (is_from) {
                    return "Bottom"
                } else {
                    return "Top"
                }
            }
            return "Center"
        }

        slideman.inject_object({
            AddGraphic: [
                this.next_id,
                {
                    TextArrow: {
                        "from": [{ x: bigInt(from_time), y: { item: from_id, anchor: anchor_from_dir(from_direction, true) } }, from_direction],
                        to: [{ x: bigInt(to_time), y: { item: to_id, anchor: anchor_from_dir(to_direction, false) } }, to_direction],
                        text: text
                    }
                }]
        })
        return this.next_id++
    }


    async add_text(
        slideman: SlideManager,
        name: string,
        time: number,
        direction: string,
        text: string,
    ): Promise<number> {
        let id = await id_of_name(name)

        slideman.inject_object({
            AddGraphic: [
                this.next_id,
                {
                    Text: {
                        "pos": [{ x: bigInt(time), y: { item: id, anchor: "Bottom" } }, direction],
                        text: text
                    }
                }]
        })
        return this.next_id++
    }

    next_id: number = 0;
}

class SlideManager {
    async slide<T>(builder: (man: SlideManager) => Promise<T>): Promise<T> {
        if (this.should_skip()) {
            inject_object({ SetViewportStrategy: "Instant" })
        } else {
            inject_object({ SetViewportStrategy: { EaseInOut: { duration: 0.5 } } })
        }
        let result = await builder(this)
        this.apply_injections()
        if (!this.should_skip()) {
            await waitingKeypress()
            history.pushState(null, "", `#${this.next_slide_number}`)
        }
        this.next_slide_number += 1;
        return result
    }

    inject_object(o: any) {
        this.injected.push(o)
    }

    apply_injections(): void {
        console.log("Applying injections");
        inject_object({ Batch: this.injected })
        this.injected = []
    }


    async pause(duration: number) {
        if (!this.should_skip()) {
            this.apply_injections()
            await sleep(duration)
        }
    }
    async short_pause() {
        await this.pause(300)
    }

    should_skip(): boolean {
        return (this.next_slide_number < this.skip_to_slide)
    }

    injected: any[] = [];

    next_slide_number: number = 0;
    skip_to_slide: number = 0;
}

async function do_presentation() {
    console.log("Starting presentation");

    let spade_state_promise = fetch(`${location.protocol}//${window.location.host}/state.ron`).then(res => res.text());

    let man = new SlideManager()
    man.skip_to_slide = parseInt(location.hash.replace("#", ""))
    let gm = new GraphicsManager();
    document.getElementById("end").style.display = 'none'

    inject_object({SetConfigFromString: CONFIG})

    inject_object({
        LoadWaveformFileFromUrl: [
            `${location.protocol}//${window.location.host}/cpu.vcd`,
            {
                keep_variables: false,
                keep_unavailable: false,
                expect_format: null,
            }

        ]
    })
    while (true) {
        if (await waves_loaded()) {
            break;
        }
        await sleep(100)
    }

    await man.slide(async (man) => {
        man.inject_object("ToggleSidePanel")
        man.inject_object("ToggleToolbar")
        man.inject_object({ AddDivider: [null, null] })
    })


    await man.slide(async (man) => {
        document.getElementById("splash").style.display = 'none'
    })

    // await waitingKeypress()

    await man.slide(async (man) => {
        man.inject_object("ToggleMenu")
        man.inject_object("ToggleToolbar")

        await man.short_pause()
        man.inject_object({ AddVariable: signal(["cpu_test_harness"], "clk") })
        await man.short_pause()
        man.inject_object({ AddVariable: signal(["cpu_test_harness"], "rst") })
        await man.short_pause()
        man.inject_object({ AddVariable: signal(["cpu_test_harness", "cpu_0"], "insn") })
        await man.short_pause()
        man.inject_object({ AddVariable: signal(["cpu_test_harness", "cpu_0"], "stall") })
        man.inject_object({ AddDivider: ["drawing_space", null] })
    })


    await man.slide(async (man) => {
        man.inject_object({ ZoomToRange: { start: bigInt(378857), end: bigInt(390403), viewport_idx: 0 } })
    })

    let clock_edge_pointers = await man.slide(async (man) => {
        let clock_edge = (time, text) => {
            man.pause(50)
            return gm.add_text_arrow(
                man,
                "cpu_test_harness.clk",
                time,
                "South",
                "drawing_space",
                384000,
                "North",
                text
            )
        };
        return [
            await clock_edge(382000, "Clock edges"),
            await clock_edge(383000, ""),
            await clock_edge(384000, ""),
            await clock_edge(385000, ""),
            await clock_edge(386000, ""),
        ]
    })

    await man.slide(async (man) => {
        man.inject_object("ToggleTickLines")
        clock_edge_pointers.map((id) => man.inject_object({ RemoveGraphic: id }))
        man.inject_object({
            VariableFormatChange: [
                {
                    field: [],
                    root: { path: { strs: ["cpu_test_harness"] }, name: "clk" }
                },
                "Clock"
            ]
        })
    })

    let arrow_id = await man.slide(async (man) => {
        man.inject_object({
            ItemBackgroundColorChange: [await index_of_name("cpu_test_harness.cpu_0.insn"), "Violet"]
        })
        let arrow_id = gm.add_text_arrow(
            man,
            "cpu_test_harness.cpu_0.insn",
            384000,
            "South",
            "drawing_space",
            384000,
            "North",
            "RISC-V Instructions"
        )
        return arrow_id
    })

    man.inject_object({ RemoveGraphic: arrow_id })

    await man.slide(async (man) => {
        man.inject_object({
            ItemBackgroundColorChange: [await index_of_name("cpu_test_harness.cpu_0.insn"), null]
        })
        man.inject_object({
            VariableFormatChange: [
                {
                    field: [],
                    root: { path: { strs: ["cpu_test_harness", "cpu_0"] }, name: "insn" }
                },
                "RV32I"
            ]
        })
    })

    await man.slide(async (man) => {
        man.inject_object({ AddDivider: [".", await index_of_name("cpu_test_harness.cpu_0.stall")] })
        man.inject_object({ ZoomToRange: { start: bigInt(376232), end: bigInt(517000), viewport_idx: 0 } })
    })

    let nop_markers = await man.slide(async (man) => {
        man.inject_object({
            VariableFormatChange: [
                {
                    field: [],
                    root: { path: { strs: ["cpu_test_harness"] }, name: "clk" }
                },
                "Bit"
            ]
        })
        let clock_edge = (time, text) => {
            man.pause(50)
            return gm.add_text_arrow(
                man,
                "cpu_test_harness.cpu_0.insn",
                time,
                "South",
                "drawing_space",
                474706,
                "North",
                text
            )
        };
        return [
            await clock_edge(382453, "NOPs"),
            await clock_edge(425414, ""),
            await clock_edge(437727, ""),
            await clock_edge(449643, ""),
            await clock_edge(461294, ""),
            await clock_edge(461294, ""),
            await clock_edge(473343, ""),
            await clock_edge(485523, ""),
            await clock_edge(494527, ""),
            await clock_edge(507502, ""),
        ]
    });

    let data_control_signals = signal(["cpu_test_harness", "test_bus_0"], "data_control_signals");
    let jump_dest = signal(["cpu_test_harness", "cpu_0"], "jump_destinataion");

    await man.slide(async (man) => {
        man.inject_object({ RemoveDisplayedItem: await id_of_name("cpu_test_harness.clk") })
        man.inject_object({ RemoveDisplayedItem: await id_of_name("cpu_test_harness.rst") })
        man.inject_object({ RemoveDisplayedItem: await id_of_name("cpu_test_harness.cpu_0.stall") })
        man.inject_object({ RemoveDisplayedItem: await id_of_name("cpu_test_harness.cpu_0.insn") })
        man.inject_object({ RemoveDisplayedItem: await id_of_name("drawing_space") })
        man.inject_object({ RemoveDisplayedItem: await id_of_name(".") })

        man.inject_object({ AddDivider: ["d1", null]})
        man.inject_object({ AddDivider: ["d2", null]})
        man.inject_object({ AddDivider: ["d3", null]})
        man.inject_object({ AddDivider: ["d4", null]})
        man.inject_object({ AddDivider: ["d5", null]})

        man.apply_injections();
    })

    await man.slide(async (man) => {
        await gm.add_text(man, "d1", 385000, "South", "New HDL")
        await gm.add_text_arrow(man, "d1", 385000, "South", "d2", 390959, "East", "Verilog/VHDL")
        await gm.add_text_arrow(man, "d2", 398707, "South", "d3", 407039, "East", "Simulator")
        await gm.add_text_arrow(man, "d3", 412301, "South", "d4", 419318, "East", "Surfer")
    })
    await man.slide(async (man) => {
        await gm.add_text_arrow(man, "d1", 384397, "South", "d4", 419025, "East", "")
        man.inject_object({ LoadSpadeTranslator: { top: "spadev::cpu::cpu_test_harness", state: await spade_state_promise } })
    })


    nop_markers.map((m) => man.inject_object({ RemoveGraphic: m }))
    await man.slide(async (man) => {
        man.inject_object({ RemoveDisplayedItem: await id_of_name("d1") })
        man.inject_object({ RemoveDisplayedItem: await id_of_name("d2") })
        man.inject_object({ RemoveDisplayedItem: await id_of_name("d3") })
        man.inject_object({ RemoveDisplayedItem: await id_of_name("d4") })
        man.inject_object({ RemoveDisplayedItem: await id_of_name("d5") })
        while (true) {
            if (await spade_loaded()) {
                break;
            }
            await sleep(100)
        }

        man.inject_object({ AddVariable: jump_dest })
        man.inject_object({
            VariableFormatChange: [
                { field: [], root: jump_dest }, "Hexadecimal"
            ]
        })
        man.inject_object({ AddVariable: data_control_signals })
        man.inject_object({
            VariableFormatChange: [
                { field: [], root: data_control_signals }, "Hexadecimal"
            ]
        })
        man.inject_object({ AddDivider: [".", null] })
        man.inject_object({ AddDivider: ["drawing_space", null] })
    })


    let jump_dest_text = await man.slide(async (man) => {
        // We need to apply here before querying the index
        man.apply_injections();

        man.inject_object({
            ItemBackgroundColorChange: [await index_of_name("cpu_test_harness.cpu_0.jump_destinataion"), "Violet"]
        })

        let jump_destinaion_code =
            `enum Option {
    Some{val: int<32>},
    None
}`
        return await gm.add_text_arrow(man, "cpu_test_harness.cpu_0.jump_destinataion", 420487, "South", "drawing_space", 420487, "North", jump_destinaion_code)
    })

    await man.slide(async (man) => {
        man.inject_object({
            ItemBackgroundColorChange: [await index_of_name("cpu_test_harness.cpu_0.jump_destinataion"), null]
        })
        man.inject_object({
            VariableFormatChange: [
                { field: [], root: jump_dest }, "spade"
            ]
        })
    })

    await man.slide(async (man) => {
        man.inject_object({ RemoveGraphic: jump_dest_text })
        man.inject_object({ ZoomToRange: { start: bigInt(381321), end: bigInt(384511), viewport_idx: 0 } })
    })



    await man.slide(async (man) => {
        man.inject_object({ ZoomToRange: { start: bigInt(376232), end: bigInt(517000), viewport_idx: 0 } })
    })

    let data_control_signals_code = await man.slide(async (man) => {
        // We need to apply here before querying the index
        man.apply_injections();

        man.inject_object({
            ItemBackgroundColorChange: [await index_of_name("cpu_test_harness.test_bus_0.data_control_signals"), "Violet"]
        })

        let data_control_signals_code =
            `struct ControlSignals {
    access_width: AccessWidth,
    addr: int<32>,
    cmd: MemoryCommand
}`
        return await gm.add_text_arrow(man, "cpu_test_harness.test_bus_0.data_control_signals", 426334, "South", "drawing_space", 426334, "North", data_control_signals_code)
    })


    await man.slide(async (man) => {
        man.inject_object({
            ItemBackgroundColorChange: [await index_of_name("cpu_test_harness.test_bus_0.data_control_signals"), null]
        })
        man.inject_object({
            VariableFormatChange: [
                { field: [], root: data_control_signals }, "spade"
            ]
        })
    })
    man.inject_object({ RemoveGraphic: data_control_signals_code })

    await man.slide(async (man) => {
        man.inject_object({ ExpandDrawnItem: { item: await id_of_name("cpu_test_harness.test_bus_0.data_control_signals"), levels: 1 } })
    })

    await man.slide(async (man) => {
        man.inject_object({
            ZoomToRange: {
                start: bigInt(429439),
                end: bigInt(444642),
                viewport_idx: 0
            }
        })
    })

    let write_prompt = async (text: string) => {
        man.inject_object({"ShowCommandPrompt": true})

        await man.pause(400)
        const chars = [...text]
        for (const c of chars) {
            man.inject_object({AddCharToPrompt: c})
            await man.pause(Math.random() * 100 + 100)
        }
    }
    await man.slide(async (man) => {
        await write_prompt("varadd alucand")
        await man.pause(750)
    })
    await man.slide(async (man) => {
        man.inject_object({ShowCommandPrompt: false})
        man.inject_object({ AddVariable: signal(["cpu_test_harness", "cpu_0"], "alu_candidates") })

        man.inject_object({ SetViewportStrategy: "Instant" })
    })
    // await man.slide(async (man) => {
    //     await write_prompt("varadd buses")
    //     man.inject_object({ShowCommandPrompt: false})
    //     man.inject_object({ AddVariable: signal(["cpu_test_harness"], "buses") })
    //     await man.pause(750)
    //     man.inject_object({ShowCommandPrompt: false})
    //     await write_prompt("exp alucand")
    //     man.inject_object({ShowCommandPrompt: false})
    //     man.inject_object({ ExpandDrawnItem: { item: await id_of_name("cpu_test_harness.cpu_0.alu_candidates"), levels: 1 } })



    // })

    await man.slide(async (man) => {
        document.getElementById("end").style.display = null
    })


}

// https://esbuild.github.io/api/#live-reload
if (!globalThis.IS_PRODUCTION)
    new EventSource('/esbuild').addEventListener('change', () => location.reload());

await do_presentation()


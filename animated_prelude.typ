#import "../typst-presentation-common/theme.typ": *
#import "../typst-presentation-common/highlight.typ": *

#set par(leading: 0.5em)

#let unfocused_color = rgb(60%, 60%, 60%)
